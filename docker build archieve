#################################################################
# Dockerfile to build bowtie2, MACS2, samtools,
# picard-tools, fastQC, bedtools, cutadapt, R, blast
# images
# Based on Ubuntu
#  $ cd ATACseqPipe.docker
#  $ VERSION=0.0.1
#  $ docker build -t yujun3315/atacseq:$VERSION .
#  $ docker images yujun3315/atacseq:$VERSION
#  $ docker push yujun3315/atacseq:$VERSION
#  $ docker tag yujun3315/atacseq:$VERSION yujun3315/atacseq:latest
#  $ docker push yujun3315/atacseq:latest
#  $ cd ~
#  $ docker pull yujun3315/atacseq:latest
#  $ mkdir tmp4atacseq
#  $ docker run -it --rm -v ${PWD}/tmp4atacseq:/volume/data \
#  $       yujun3315/atacseq:latest bash
##################################################################

# Set the base image
FROM continuumio/miniconda:4.5.11
#LABEL authors="jyu@wuxinextcode.com" \
#      description="Docker image to run atacseq pipeline"

# File/Author / Maintainer
MAINTAINER Jun Yu <junyu@bu.edu>

# Install procps so that Nextflow can poll CPU usage
# Update the repository sources list, install wget, unzip, curl, git
RUN \
  apt-get update --fix-missing && \
  apt-get install --yes wget procps git bzip2 ca-certificates curl unzip gdebi-core git rsync libssl-dev libcurl4-openssl-dev libgsl-dev zlib1g-dev ttf-dejavu g++ && \
  apt-get clean -y
## rm -rf /var/lib/apt/lists/*

###RUN apt-get update && apt-get install -y procps && apt-get clean -y
## RUN conda install conda=4.5.11
COPY environment.yml /
RUN conda env create -f /environment.yml && conda clean -a
ENV PATH /opt/conda/envs/atacseq1.0/bin:$PATH


environment.yml
# You can use this file to create a conda environment for this pipeline:
#   conda env create -f environment.yml
name: atacseq1.0
channels:
  - bioconda
  - conda-forge
  - r
  - defaults
dependencies:
  - conda-forge::openjdk=8.0.144 # Needed for FastQC - conda build hangs without this
  - fastqc=0.11.8
  - trim-galore=0.5.0
  - bwa=0.7.17
  - bowtie2=2.3.4
  - ucsc-bedgraphtobigwig=366
  - ucsc-bedclip=366
  - conda-forge::gawk=4.2.1
  - bioconda::r-spp=1.15.2
  - bioconda::phantompeakqualtools=1.2
  - bioconda::macs2=2.1.2
  - bedtools=2.27.1
  - samtools=1.9
  - picard=2.18.7
  - deeptools=3.1.3

